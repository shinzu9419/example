<?php

namespace Recipe\Library;

use Zend\ServiceManager\ServiceManager;
use Invi\Pdf;
use \Recipe\Model\Recipe;
use \Recipe\Model\RecipeElement;
use \Recipe\Model\RecipeSettings;
use Visit\Library\Abstracts\PrinterAbstract;

/**
 * Recpie is tool class use for generete and probject Pdf with Recipe
 * @module RecipeLib
 * @modulegroup Panel
 * @author Oskar Sęk
 */
class RecipeLib extends PrinterAbstract
{

    protected $page_format;
    protected $recipes;
    protected $pdf;
    protected $sm;
    protected $oldParam;
    
    protected $printData = array();
    protected $printAll = false;
    protected $toPrint = array();
    protected $recipeId = false;

    /**
     * @access public
     * @param $recipeId int
     * is reference to serviceLocator
     * @param Pdf $tcpdf
     * is object of Pdf automaticaly initialized by default
     */
    function __construct($recipeId = false)
    {
        $this->recipeId = $recipeId;
    }

    /**
     * 
     * @access public
     * @return PDF
     */
    public function getPdfObject()
    {
        return $this->pdf;
    }

    protected function getPageFormat()
    {
        return $this->page_format;
    }

    /**
     * @access protected
     * This method you can use for get data of the Recipes with the specified visit id
     * @param $id
     * is id of visit
     * @return Array 
     */
    protected function getData($id)
    {
        $model = new \Recipe\Model\Recipe($this->sm);
        $select = $model->getSelectListByVisit($id);
        $data = $model->getTableGateway()->selectWith($select);
        $recipe = $data->toArray();

        return $recipe;
    }

    /**
     * This method uses for ada sigle recipe to Pdf object
     * @param $dataRecipe is data of recipe
     * @param RecipeSettings $modelConfig is object with data of doctor
     * @param $elementsRecipe is
     * @param type $countRecipe
     * @access protected
     */
    protected function addRecipe($dataRecipe, RecipeSettings $modelConfig, $elementsRecipe, $countRecipe = 0)
    {
        $this->pdf->SetFillColor(255, 255, 255);

        $xPosition = $countRecipe % 3;
        if ($countRecipe % 3 == 0) {
            $this->pdf->AddPage('L', $this->getPageFormat(), false, false);
        }

        $marginTop = 5;
        $marginLeft = 8 + (92 * $xPosition);

        $this->addToPrintHeader($dataRecipe, $modelConfig, $marginTop, $marginLeft);

        $this->addToPrintPatient($dataRecipe, $marginTop, $marginLeft);

        $this->addToPrintNFZSection($dataRecipe, $marginTop, $marginLeft);

        $this->addDragsToPrint($elementsRecipe, $marginTop, $marginLeft);

        $this->pdf->SetFontSize(8);
        $this->pdf->write1DBarcode($dataRecipe->number, 'C128', $marginLeft, (150 + $marginTop), 80, 10, 0.6, '', 'N');
        $this->addToPrintFooter($dataRecipe, $marginTop, $marginLeft);
    }

    /**
     * This method uses for print page
     */
    public function output()
    {
        $this->pdf->Output('recepty.pdf', 'I');
    }

    public function addRecipreToPdfFromId($id, $countRecipe = 0)
    {
        $model = new \Recipe\Model\Recipe($this->sm);
        $recipeTable = $model->getRicipeById($id);
        foreach ($recipeTable as $recipe) {
            $model = new \Recipe\Model\Recipe($this->sm);
            $modelElements = new \Recipe\Model\RecipeElement($this->sm);
            $modelConfig = new \Recipe\Model\RecipeSettings($this->sm);

            $dataRecipe = $model->getRow($recipe['id']);
            $modelPatient = new \Patient\Model\Patient($this->sm);

            $userData = $this->getUserDataAndUpdateRecipe($dataRecipe->patient_id, $model, $recipe['id'], $modelPatient);

            $dataRecipe = $model->getRow($recipe['id']);

            $elementsRecipe = $modelElements->getElementByRecipe($dataRecipe['id']);


            $this->addRecipe($dataRecipe, $modelConfig, $elementsRecipe);
        }
    }

    protected function setOlderParam()
    {
        $this->oldParam['printHeader'] = true;
        $this->oldParam['printFooter'] = true;

        // set default monospaced font
//        $this->pdf->GetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $this->oldParam['margin'] = $this->pdf->getMargins();
        $this->oldParam['marginHeader'] = $this->pdf->GetHeaderMargin();
        $this->oldParam['marginFooter'] = $this->pdf->GetFooterMargin();

        // set auto page breaks
        $this->oldParam['autoPageBreak'] = $this->pdf->GetAutoPageBreak();

        $pagePref = array(
            'PrintScaling' => 'None', // None, AppDefault
        );
        $this->pdf->setViewerPreferences($pagePref);

        return $this->pdf;
    }

    protected function setOlderToActualParam()
    {
        $this->pdf->setPrintHeader($this->oldParam['printHeader']);

        // set default monospaced font
        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $this->pdf->SetMargins($this->oldParam['margin']['left'], $this->oldParam['margin']['top'], $this->oldParam['margin']['right']);
        $this->pdf->SetHeaderMargin($this->oldParam['marginHeader']);
        $this->pdf->SetFooterMargin($this->oldParam['marginFooter']);

        // set auto page breaks
        $this->pdf->SetAutoPageBreak($this->oldParam['autoPageBreak']);


        $pagePref = array(
            'PrintScaling' => 'None', // None, AppDefault
        );
        $this->pdf->setViewerPreferences($pagePref);
    }

    protected function setBasicParam()
    {
        $this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(false);

        // set default monospaced font
        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $this->pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);
        $this->pdf->SetMargins(0, 0, 0);
        $this->pdf->SetHeaderMargin(0);
        $this->pdf->SetFooterMargin(0);

        // set auto page breaks
        $this->pdf->SetAutoPageBreak(TRUE, 5);


        $pagePref = array(
            'PrintScaling' => 'None', // None, AppDefault
        );
        $this->pdf->setViewerPreferences($pagePref);

        return $this->pdf;
    }

    protected function addToPrintHeader($dataRecipe, $modelConfig, $marginTop, $marginLeft)
    {
        //Nagłówek
        $swiadczniobiorca = "{$modelConfig->name}\n REGON: {$modelConfig->regon} \n {$modelConfig->address}, "
                . "{$modelConfig->phone}, ";

        $this->pdf->SetFontSize(10);
        $this->pdf->MultiCell(90, 34, "Recepta {$dataRecipe->number}", 1, 'L', 1, 0, ($marginLeft - 5), $marginTop, true);
        $this->pdf->SetFontSize(10);
        $this->pdf->MultiCell(90, 34, $swiadczniobiorca, 0, 'C', 1, 0, ($marginLeft - 5), (5 + $marginTop), true);
        $this->pdf->SetFontSize(8);
        $this->pdf->write1DBarcode($dataRecipe->regon, 'C128', (23 + $marginLeft), (23 + $marginTop), 80, 10, 0.6, '', 'N');
        $this->pdf->MultiCell(90, 34, 'Świadczeniodawca', 1, 'L', 1, 0, ($marginLeft - 5), $marginTop, true, 0, false, true, 34, 'B');
    }

    protected function addToPrintPatient($dataRecipe, $marginTop, $marginLeft)
    {
        //Pacjent
        $patientName = "{$dataRecipe->patient_first_name} {$dataRecipe->patient_last_name} ";
        $patientAddr = "{$dataRecipe->patient_address}";
        $this->pdf->MultiCell(90, 30, '', 1, 'L', 1, 0, ($marginLeft - 5), (34 + $marginTop), true);
        $this->pdf->MultiCell(68, 30, 'Pacjent', 1, 'L', 1, 0, ($marginLeft - 5), (34 + $marginTop), true);
        $this->pdf->SetFontSize(10);
        $this->pdf->MultiCell(68, 10, $patientName, 0, 'C', 1, 0, ($marginLeft - 5), (39 + $marginTop), true);
        $this->pdf->MultiCell(68, 10, $patientAddr, 0, 'C', 1, 0, ($marginLeft - 5), (43 + $marginTop), true);
        $this->pdf->SetFontSize(8);
        $this->pdf->MultiCell(68, 30, 'Pesel', 1, 'L', 1, 0, ($marginLeft - 5), (34 + $marginTop), true, 0, false, true, 30, 'B');
        if (!empty($dataRecipe->patient_pesel))
            $this->pdf->write1DBarcode($dataRecipe->patient_pesel, 'C128', (7 + $marginLeft), (52 + $marginTop), 55, 8, 0.6, '', 'N');
        if (!empty($dataRecipe->patient_pesel))
            $this->pdf->MultiCell(68, 30, $dataRecipe->patient_pesel, 1, 'R', 1, 0, ($marginLeft - 5), (34 + $marginTop), true, 0, false, true, 30, 'B');
    }

    protected function addToPrintNFZSection($dataRecipe, $marginTop, $marginLeft)
    {
        //NFZ
        $this->pdf->MultiCell(22, 17, 'Oddział NFZ', 1, 'L', 1, 0, (63 + $marginLeft), (34 + $marginTop), true);
        $this->pdf->MultiCell(20, 17, $dataRecipe->nfz, 0, 'C', 1, 0, (64 + $marginLeft), (40 + $marginTop), true);

        $this->pdf->SetFontSize(6);
        $this->pdf->MultiCell(22, 13, 'Uprawnienia dodatkowe', 1, 'L', 1, 0, (63 + $marginLeft), (51 + $marginTop), true);
        $this->pdf->SetFontSize(8);
        $this->pdf->MultiCell(20, 17, $dataRecipe->uprawnienia_dodatkowe, 0, 'C', 1, 0, (64 + $marginLeft), (57 + $marginTop), true);
    }

    protected function addDragsToPrint($elementsRecipe, $marginTop, $marginLeft)
    {
        // pozycje leków
        $this->pdf->MultiCell(90, 100, 'Rp', 1, 'L', 1, 0, ($marginLeft - 5), (64 + $marginTop), true);
        $this->pdf->MultiCell(90, 100, 'Odpłatność     ', 1, 'R', 1, 0, ($marginLeft - 5), (64 + $marginTop), true);

        $rowY = (69 + $marginTop);
        $intervalY = 13;
        $this->pdf->SetFontSize(9);
        $correction = 3;
        foreach ($elementsRecipe as $element) {

            $nz = ($element['nz'] == 'yes') ? 'NZ' : '';

            $this->pdf->MultiCell(66, 12, "{$element['nazwa']}. {$element['opakowanie']}. ", array('TB' => array(
                    'dash' => 1
                )), 'L', 1, 0, ($marginLeft - $correction), $rowY, true);

            $dawka = (!empty($element['dawka'])) ? "Dawka: {$element['dawka']}" : '';
            $this->pdf->MultiCell(64, 3, "{$dawka}", 0, 'L', 1, 0, ($marginLeft - $correction), $rowY + 4, true);

                $dawkowanie = array();
                if (!empty($element['dawkowanie'])) $dawkowanie[0] = "Dawkowanie: {$element['dawkowanie']}";
                if (!empty($nz)) $dawkowanie[1] = $nz;
                
                $this->pdf->MultiCell(64, 3, implode(", ", $dawkowanie), 0, 'L',
                1, 0, ($marginLeft - $correction), $rowY + 8, true);

                $this->pdf->MultiCell(21, 12, "{$element['odplatnosc']}", 
                array('TLB' => array('dash' => 1)), 'C', 1, 0,
                (63 + $marginLeft), $rowY, true);

                $rowY = $rowY + $intervalY;
        }
    }

    protected function addToPrintFooter($dataRecipe, $marginTop, $marginLeft)
    {
        $this->addToPrintDate($marginTop, $marginLeft, $dataRecipe);
        $this->addToPrintDescription($marginTop, $marginLeft, $dataRecipe);

        $this->pdf->write1DBarcode($dataRecipe->doctor_pwz, 'C128', 
        (45 + $marginLeft), (180 + $marginTop), 35, 8, 0.6, '', 'N');
        $this->pdf->SetFontSize(8);
    }

    protected function addToPrintDate($marginTop, $marginLeft, $dataRecipe)
    {
        // data wystawienia
        $this->pdf->MultiCell(47, 16, 'Data wystawienia', array('LTRB' => array(
                'dash' => 0
            )), 'L', 1, 0, ($marginLeft - 5), (164 + $marginTop), true);

        $this->pdf->SetFontSize(10);

        $this->pdf->MultiCell(44, 16, $dataRecipe->date_create, 0, 'L', 1, 0,
        $marginLeft, (170 + $marginTop), true);
        $this->pdf->SetFontSize(8);

        // data realizacji
        $this->pdf->MultiCell(47, 16, 'Data realizacji od dnia', 1, 'L', 1, 0,
        ($marginLeft - 5), (180 + $marginTop), true);
        $this->pdf->SetFontSize(10);
        
        $date = $dataRecipe->date_completion;
        if(!isset($dataRecipe->date_completion) || strlen($dataRecipe->date_completion) == 0){
            $date = 'X';
        }

        $this->pdf->MultiCell(44, 9, $date, 0, 'L', 1, 0,
        $marginLeft, (186 + $marginTop), true);
        $this->pdf->SetFontSize(8);
    }

    protected function addToPrintDescription($marginTop, $marginLeft, $dataRecipe)
    {
        //podpis lekarza
        $this->pdf->MultiCell(43, 32, 'Dane i podpis lekarza', 1, 'L', 1, 0, 
        (42 + $marginLeft), (164 + $marginTop), true);
        $this->pdf->MultiCell(43, 32, 'Wydruk własny', 1, 'R', 1, 0, 
        (42 + $marginLeft), (164 + $marginTop), true, 0, false, true, 32, 'B');

        $this->pdf->SetFontSize(10);

        $this->pdf->MultiCell(40, 10, $dataRecipe->doctor_name, 0, 'L', 1, 0, 
        (43 + $marginLeft), (170 + $marginTop), true);
        $this->pdf->MultiCell(40, 10, 'PWZ: ' . $dataRecipe->doctor_pwz, 
        0, 'L', 1, 0, (43 + $marginLeft), (174 + $marginTop), true);
    }

    protected function initTCPDF()
    {
        $this->pdf = new Pdf('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    }

    protected function getOldPageFormat()
    {
        $this->page_format = array(
            'MediaBox' => array('llx' => 0, 'lly' => 0, 'urx' => 297, 'ury' => 210),
            'Dur' => 3,
            'trans' => array(
                'D' => 1.5,
                'S' => 'Split',
                'Dm' => 'V',
                'M' => 'O'
            ),
            'Rotate' => 0,
            'PZ' => 1,
        );
        return $this->page_format;
    }

    protected function setPageFormat()
    {
        $this->page_format = array(
            'MediaBox' => array('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 297),
            'Dur' => 3,
            'trans' => array(
                'D' => 1.5,
                'S' => 'Split',
                'Dm' => 'V',
                'M' => 'O'
            ),
            'Rotate' => 0,
            'PZ' => 1,
        );
    }
    
    public function getRecipeDoctorData()
    {
        $modelConfig = new \Recipe\Model\RecipeSettings($this->sm);

        $recipe = array();
        $recipe['name'] = $modelConfig->name;
        $recipe['doctor_pwz'] = $modelConfig->doctor_pwz;
        $recipe['address'] = $modelConfig->address;
        $recipe['phone'] = $modelConfig->phone;
        $recipe['regon'] = $modelConfig->regon;

        return $recipe;
    }

    public function saveRecipe($data, $patient)
    {
        $dbRecipe = new Recipe($this->sm);
        foreach ($data as $recipe) {
            
            if (!$this->recipeHasDrugs($recipe)) {
                continue;
            }

            if ($recipe['id'] == 0) {
                $recipeDoc = $this->getRecipeDoctorData();
                $recipe += $recipeDoc;
                $recipe['doctor_pwz'] = $recipeDoc['doctor_pwz'];

                $recipe['patient_first_name'] = $patient['first_name'];
                $recipe['patient_last_name'] = $patient['last_name'];
                $recipe['patient_address'] = "{$patient['street']} {$patient['street_no']}";
                if (isset($patient['street_nr']) && strlen($patient['street_nr']) != 0) {
                    $recipe['patient_address'] .= "/{$patient['street_nr']}";
                }
                $recipe['patient_address'] .= ",{$patient['zip']} {$patient['city']}";
                $recipe['patient_pesel'] = $patient['pesel'];

                unset($recipe['id']);

                $recipe['number'] = $this->prepareRecipeNumber();

                $recipeId = $dbRecipe->addRow($recipe);
                $recipe['id'] = $recipeId;
            } else {
                $dbRecipe->updateRow($recipe['id'], $recipe);
            }
            $this->saveRecipeElement($recipe);
        }
    }
    
    private function recipeHasDrugs($recipe)
    {
        $drugs = [];
        foreach ($recipe['drug'] as $drug) {
            if ($drug !== null && $drug['id'] !== null)
                array_push($drugs, $drug);
        }
        return (count($drugs) > 0);
    }


    public function saveRecipeElement($recipe)
    {
        $dbDrug = new RecipeElement($this->sm);
        foreach ($recipe['drug'] as $drug) {
            if ($drug['id'] == 0) {
                unset($drug['id']);
                if ($drug['nazwa'] !== '' || strlen($drug['nazwa']) != 0) {
                    $drug['recipe_id'] = $recipe['id'];
                    $drugId = $dbDrug->addRow($drug);
                    $drug['id'] = $drugId;
                }
            } else {
                $dbDrug->updateRow($drug['id'], $drug);
            }
        }
    }

    /**
     * 
     * @return $recipeNumber
     * @throws \Exception - if there's no free number
     */
    public function prepareRecipeNumber()
    {
        $recipeNumberDb = new \Recipe\Model\RecipeRangeNumber($this->sm);
        $recipeNumber = $recipeNumberDb->getFreeNumber();
        if ($recipeNumber == 0) {
            throw new \Exception("Nie ma wolnych numerów recept");
        } else {
            return $recipeNumber;
        }
    }

    public function render()
    {
        $this->setOlderParam();
        $this->setPageFormat();
        $this->setBasicParam();
        
        $recipeDb = new \Recipe\Model\Recipe($this->sm);
        $recipeList = $recipeDb->getRecipeListByVisit($this->visit['id']);
        
        $modelElements = new RecipeElement($this->sm);
        $modelConfig = new RecipeSettings($this->sm);

        $countRecipe = 0;
        if(!$this->recipeId) {           
            foreach ($recipeList as $index => $print) {

                if (!$this->canPrint($index, $print))
                {
                    continue;

                } else {
                    $dataRecipe = $recipeDb->getRow($print['id']);
                    $elementsRecipe = $modelElements->getElementByRecipe($print['id']);

                    $this->addRecipe($print, $modelConfig, $elementsRecipe, $countRecipe);
                    
                    $countRecipe++;
                }
            }
        } else {
            $recipes = $recipeDb->getRow($this->recipeId);
            $elementsRecipe = $modelElements->getElementByRecipe($this->recipeId);

            $this->addRecipe($recipes, $modelConfig, $elementsRecipe, $countRecipe);
        }
        $this->setOlderToActualParam();
    }

    protected function canPrint($index, $print) 
    {
        if (empty($this->printData)) {

            try {

                $form = $this->getForm();
                if ($form) {
                    $formData = $this->getForm()->getData();
                    $this->printData = $formData['recipe'];

                    $this->printAll = true;
                    foreach ($this->printData as $row) {
                        if ($row['print_selected'] == 1) {
                            $this->printAll = false;
                            break;
                        }
                    }
                }
            } catch (\Exception $exc) {    
            }
        }

        $canPrint = false;
        if (isset($this->printData[$index])) {
            $canPrint = $this->printData[$index]['print_selected'] == 1 || $this->printAll == true;
        } else {
            $canPrint = true;
        }

        return $canPrint;
    }

}
