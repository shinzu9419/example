<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NfzController
 *
 * @author Oskar
 */
namespace Patient\Controller;

use Invi\Mvc\Controller\ActionController;
use Patient\Library\Ewus\LibEwus;
use Invi\System\JSON;
use Visit\Model\Visit;
use Patient\Model\NfzEwusConfirm;
use Patient\Grid\HistoryEwus;
use Patient\Grid\HistoryEwusFull;

class NfzController extends ActionController
{
    protected $events = false;
    
    /**
     *
     * @var NfzEwusConfirm; 
     */
    protected $dbNfzEwus = false;
    
    public function indexAction()
    {
        
    }
    
    public function patientStatusAction()
    {
        //Dorobić obsługę błędów
        $patientId = $this->params()->fromRoute('id');
        if($patientId){
            $lib = new LibEwus($this->getServiceLocator());
            $array = $lib->checkPatientStatus($patientId);
            
            
            if ($array['data']) {
                $model = new NfzEwusConfirm($this->getServiceLocator());

                $model->addRow(array('patient_id' => $patientId, 'ewus_confirm_date' => $array['data'][64],
                    'ewus_response_time' => date('Y-m-d H:i:s')));
                
                return $this->json(JSON::good(array
                    ('patient' =>  $array['data'],
                    'session' => array('code' => $array['msgC'],
                        'msg' =>  $array['msg']))),
                    true);
            } else if($array['session']){
                return $this->json(JSON::good(array
                    ('patient' =>  $array['patient'],
                    'session' => $array['session'])),
                    true);
            }
            
        }
        return $actionController->json(JSON::error(), true);
    }
    
    public function dbTestAction(){
        $model = new \Patient\Model\NfzEwusConfirm($this->getServiceLocator());
        $result = $model->getRowsByPatientId(8846);
        var_dump($result->toArray());
        $result = $model->patientIsInsured(8846);
        var_dump($result);
        
    }
    
 
}
