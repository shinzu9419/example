<?php

namespace Patient\Model;

use Zend\Db\TableGateway\TableGateway;
use Invi\System\AbstractDbTable;
use Zend\Db\Sql\Expression;

class NfzEwusConfirm extends AbstractDbTable
{
    /* @var $_tableGateway TableGateway */

    protected $_name = 'nfz_ewus_confirm';


    public function getRowsByPatientId($id)
    {
        $select = $this->getSelect();
        $select->where->equalTo('patient_id', $id);
        $select->order('ewus_response_time DESC');
        $result = false;
        try{
            $result = $this->fetchAll($select);
        } catch(\Exception $ex){
            $result = false;
        }
        return $result;
    }
    
    public function getConfirmRowByPatientId($id)
    {
        $select = $this->getSelect();
        $select->where->equalTo('patient_id', $id);
        $select->where->greaterThanOrEqualTo('ewus_confirm_date', date('Y-m-d').' 00:00:00');
        $select->order('ewus_response_time DESC');
        $result = false;
        try{
            $result = $this->fetchAll($select);
        } catch(\Exception $ex){
            $result = false;
        }
        return $result;
    }

    public function patientIsInsured($id)
    {
        
        $data = $this->getConfirmRowByPatientId($id);
        if($data->count()) {
            $result = true;
        } else {
            $result = false;
        }

        return $result;
    }
    
    public function getFullHistoryList(){
        $select = $this->getSelect();
        $select->columns(array('id', 'patient_id', 'ewus_confirm_date', 'ewus_response_time'));
        $select->join(array('p' => 'patient'), 'p.id = patient_id', array('first_name', 'last_name', 'pesel'));

        return $select;
    }
    
    public function getPatintHistoryList($id){
        $select = $this->getSelect();
        $select->columns(array('id', 'patient_id', 'ewus_confirm_date', 'ewus_response_time'));
        $select->join(array('p' => 'patient'), 'p.id = patient_id', array('first_name', 'last_name', 'pesel'));
        $select->where->equalTo('id', $id);

        return $select;
    }
    
    /**
     * 
     * @param int $id
     * @return type
     */
    public function getHistoryList(int $id){
        $select = $this->getSelect();
        $select->where->equalTo('nfz_ewus_confirm.patient_id.patient.id', $id)->join(array('p' => 'patient'),  'nfz_ewus_confirm.patient_id = p.id');

        
        return $select;
    }

    public function deleteRow($id)
    {
        
    }

    public function updateRow($id, $data)
    {

    }

}
