<?php
namespace Patient\Library\Ewus;

use Zend\ServiceManager\ServiceLocatorInterface;

class Lib {
        
    protected static $LibEwus = false;
    
    protected $isDevel = NULL;
            
    protected static $serviceLocator = false;



    protected static $_domainList = array(
        '01' => array('name' => 'Dolnośląski',
            'hasType' => true),
        '02' => array('name' => 'Kujawsko-Pomorski',
            'hasType' => false),
        '03' => array('name' => 'Lubelski',
            'hasType' => false),
        '04' => array('name' => 'Lubuski',
            'hasType' => true),
        '05' => array('name' => 'Łódzki',
            'hasType' => true),
        '06' => array('name' => 'Małopolski',
            'hasType' => true),
        '07' => array('name' => 'Mazowiecki',
            'hasType' => false),
        '08' => array('name' => 'Opolski',
            'hasType' => true),
        '09' => array('name' => 'Podkarpacki',
            'hasType' => true),
        '10' => array('name' => 'Podlaski',
            'hasType' => false),
        '11' => array('name' => 'Pomorski',
            'hasType' => true),
        '12' => array('name' => 'Śląski',
            'hasType' => true),
        '13' => array('name' => 'Świętokrzyski',
            'hasType' => false),
        '14' => array('name' => 'Warmińsko-Mazurski',
            'hasType' => false),
        '15' => array('name' => 'Wielkopolski',
            'hasType' => false),
        '16' => array('name' => 'Zachodniopomorski',
            'hasType' => false),
        '17' => array('name' => 'Centrala',
            'hasType' => false),
    );
    
    public static function getDomainList(){
        return self::$_domainList;
    }
    
    public static function getDomainListForSelect(){
        
        $domainList = array();
        
        foreach(self::$_domainList as $key => $value){
            $domainList[$key] = "{$value['name']}({$key})";
        }
        
        return $domainList;
    }
    public static function ForJsShowAndHide($value){
            ?>  if(<?php if($value['hasType']):?>true<?php elseif(true):?>false<?php endif; ?>){
                $typeGruop.show();
                $codeGruop.show();
            }
            else{
                $typeGruop.hide();
                $codeGruop.hide();
            }
        <?php
    }
    public static function ForJsChangeAndStart(){
        $domainList = self::getDomainList();
        $i = 0; foreach ($domainList as $key => $value):
            if($i === 0):
    ?>if($select.val() == <?php echo $key ?>){
        <?php self::ForJsShowAndHide($value); 
    ?>}<?php else:?>else if($select.val() == <?php echo $key ?>){
         <?php self::ForJsShowAndHide($value); 
    ?>}
            <?php endif; ?>
        <?php $i++; endforeach;
    }

}
