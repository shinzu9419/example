<?php

/*
 * @author Maciej "Gilek" Kłak
 * @copyright Copyright &copy; 2014 Maciej "Gilek" Kłak
 * @version 1.1a
 * @package ewus
 */

namespace Patient\Library\Ewus\Exception;

class ServerException extends Exception
{
    
}
