<?php
namespace Patient\Library\Ewus;

use Invi\System\JSON;
use Patient\Model\Patient;
use Patient\Library\Ewus\Client;
use Auth\Model\UserNfz;
use Patient\Library\Ewus\Session;
use Zend\ServiceManager\ServiceLocatorInterface;
use Invi\Mvc\Controller\ActionController;
use Application\Model\Settings;
use Patient\Library\Ewus\Exception\SessionIdException;
use Patient\Library\Ewus\Exception\TokenException;
use Patient\Library\Ewus\Exception\ServiceException;

class LibEwus {
        
    protected static $LibEwus = false;
    
    protected $isDevel = NULL;
            
    protected $serviceLocator = false;
    
    protected $userId = false;
    
    protected $client = false;
    
    protected $session = false;
    
    protected $dbUserNfz = false;
    
    protected $dbSettings = false;



    public function __construct(ServiceLocatorInterface $sm){
        $this->serviceLocator = $sm;
    }
    

    
    /**
     * Retrieve serviceManager instance
     *
     * @return ServiceLocatorInterface
     */
    protected function getServiceLocator(){
        return $this->serviceLocator;
    }

    
    /**
     * 
     * @return bool
     */
    public function _IsDevelVersion()
    {
        if(is_null($this->isDevel)){
            $config = $this->getServiceLocator()->get('Config');
            $this->isDevel = $config['ewus']['develInstance'];
        }
        return $this->isDevel;
    }
    
    /**
     * 
     * @return int
     */
    protected function _getActUser()
    {
        if(!$this->userId){
            $sm = $this->getServiceLocator();
            $auth = $sm->get('Invi\Auth');
            $user = $auth->getStorage()->read();
            $this->userId = $user['id'];
        }
        return $this->userId;
    }
    
     /**
     * 
     * @return Client
     */
    protected function _getClient()
    {
        if(!$this->client){
            $this->client = new Client($this->_IsDevelVersion());
        }
        return $this->client;
    }
    
    /**
     * 
     * @param array $param
     * @return Session
     */
    protected function getSession()
    {
        if(!$this->session){
            $ewusData = $this->getEwusDataFromSettings();
            
            $param = $this->getParam($ewusData['ewus_domain'],
                    $ewusData['ewus_type'], $ewusData['ewus_code']);
            
            $this->session = $this->_getClient()->login($ewusData['ewus_login'],
                $ewusData['ewus_pass'], $param);
        }
        return $this->session;
    }
    
    public function getEwusDataFromSettings()
    {
        $dbSettings = new Settings($this->getServiceLocator());
        
        $login = $dbSettings->getRow('ewus_login')->value;
        $pass = $dbSettings->getRow('ewus_pass')->value;
        $domain = $dbSettings->getRow('ewus_domain')->value;
        $type = $dbSettings->getRow('ewus_type')->value;
        $code = $dbSettings->getRow('ewus_code')->value;
        
        $param = array('ewus_login' => $login, 'ewus_pass' => $pass,
            'ewus_domain' => $domain, 'ewus_type' => $type,'ewus_code' => $code);
        
        return $param;
    }
    
    public function getEwusData()
    {
        $userId = $this->_getActUser();
        $model = $this->getDbUserNfz();
        return $model->getByUserId($userId);
    }
    
    protected function getParam($domain, $type, $code)
    {
        $param = array('domain' => $domain);
        if(strlen($type) > 0){
            $param['type'] = $type;
            if(strlen($code) > 0){
                if($type == 'SWD'){
                    $key = 'idntSwd';
                }
                elseif($type == 'LEK'){
                    $key = 'idntLek';
                }
                $param[$key] = $code;
            }
        }
        return $param;
    }
    
    /**
     * 
     * @return array
     */
    public function checkPatientStatus($patientId)
    {
        $sm = $this->getServiceLocator();
        $dbPatient = new Patient($sm);
        
        $patient = $dbPatient->getRow($patientId);
        
        $pesel = $patient->pesel;
        
        try{
            $session = $this->getSession();
            $msgC = $session->getLoginMessageCode();
            $msg = $session->getLoginMessage();

            $response = $session->checkCWU($pesel);
            $data = $response->getData();
            return array('data' => $data, 'msg' => $msg, 'msgC' => $msgC);
        } catch (ServiceException $ex){
            $session = array('patient' => array('1' => '100'), 'session' => array('code' => '100'));
        } catch (SessionIdException $ex){
            $session = array('patient' => array('1' => '100'), 'session' => array('code' => '102'));
        } catch (TokenException $ex){
            $session = array('patient' => array('1' => '100'), 'session' => array('code' => '103'));
        } catch (\Exception $ex){
            $session = array('patient' => array('1' => '100'), 'session' => array('code' => '101'));
        }

        if($session['session']['code'] == '100' ||
            $session['session']['code'] == '101'||
            $session['session']['code'] == '102'||
            $session['session']['code'] == '103') {

            return $session;
        }
        return false;
    }
    
    protected function unsetClientAndSession(){
        $this->session = false;
        $this->client = false;
    }

    public function changePass($pass)
    {
        $session = $this->getSession();
        
        try{
            $result = $session->changePassword($pass);
        } catch (Exception\ResponseException $ex) {
            var_dump($ex);
            var_dump("tot")
            ?>
            <script>
                jQuery(document).ready(function($) {
                        inviNotification.error('Wystąpił błąd przy rozpoczęciu wizyty');
                });
            </script>
            <?php
        }
        
        if(!$this->_IsDevelVersion()){
            $this->_setPassInDb($pass);
        }
        $this->unsetClientAndSession();
    }

    
    protected function _setPassInDb($pass){
        $model = $this->getDbSettings();
        $data = $model->getRow($userId);
        $model->updateRow('ewus_pass', array('value' => $pass));
    }
    
    /**
     * 
     * @return UserNfz
     */
    protected function getDbUserNfz(){
        if(!$this->dbUserNfz){
            $this->dbUserNfz = new UserNfz($this->getServiceLocator());
        }
        return $this->dbUserNfz;
    }
    /**
     * 
     * @return Settings
     */
    protected function getDbSettings(){
        if(!$this->dbSettings){
            $this->dbSettings = new Settings($this->getServiceLocator());
        }
        return $this->dbSettings;
    }
    
    protected function getActionController(){
        return self::$controler;
    }
    
    protected function reconectEwus(){
        $this->getSession();
    }
}
